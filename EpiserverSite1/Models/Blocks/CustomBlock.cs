﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace EpiserverSite1.Models.Blocks
{
    [ContentType(DisplayName = "Custom ",
        GUID = "0620924f-8dbc-456b-bc9e-dafff00cb068",
        Description = "Blocks: configure unified blocks to display content.")]

    public class CustomBlock : BlockData
    {
        
                [CultureSpecific]
                [Display(
                    Name = "Main body",
                    Description = "Name field's description",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual XhtmlString MainBody { get; set; }
         
    }
}